//
//  LoginService.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 3/26/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import KeychainSwift

protocol LoginServiceDelegate: class {
    func loginSuccessful(from: String)
    func loginFailed(errorMessage: String)
}

class LoginService {
    weak var delegate : LoginServiceDelegate?
    
    func signInWithMobileNumber() {
        
        self.delegate?.loginSuccessful(from: "SignIn")
    }
    
    func signUpWithMobileNumber() {
        
        let keychain = KeychainSwift()
        var mail = String()
        let appVersion = version()
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        if let eMail = keychain.get(Keys_General.Email_Address) {
            mail = eMail
        } else {
            mail = "Not Registered"
        }
        
        let num = String(describing: keychain.get(Keys_General.Mobile_Number)!)
        let mobile = num.suffix(10)
        let countryCode = num.components(separatedBy: mobile)
        
        var countryName = String()
        if let index = countryCodeDictionary.values.index(of: String(describing: countryCode[0])) {
            countryName = countryCodeDictionary.keys[index]
        }
        
        let userLat = Double(UserDefaults.standard.double(forKey: Keys_General.Location_Latitude)).rounded(toPlaces: 2)
        let userLng = Double(UserDefaults.standard.double(forKey: Keys_General.Location_Longitude)).rounded(toPlaces: 2)
        
        var currencyID = Int()
        if countryName == "India" {
            currencyID = 43
        } else {
            currencyID = 86
        }
        
        let json = ["accountTypeId" : 1,
        "mobile" : mobile,
        "countryCode" : countryCode[0],
        "fbId" : "asdkjhkh218973987",
        "firstName" : "Naren",
        "email" : mail,
        "countryName" : countryName,
        "currencyId" : currencyID,
        "deviceId" : deviceID,
        "userLat"    : userLat,
        "userLng"    : userLng,
        "os"    : 2,
        "appVersion"    : appVersion,
        "notificationToken" : "027e1fc722f044b534234f8bce9830a700b412fddc69839557ab88ac305d9dc2"] as [String : Any]
        
        print(json)
        
        NetworkManager().apiCall(url: API.SIGNUP_URL, http_method: HTTP_METHOD_POST, dict_body: json as NSDictionary, getParameters: nil) { (responseDictionary) in
            print(responseDictionary)
        }
        
        self.delegate?.loginSuccessful(from: "SignUp")
    }
}
