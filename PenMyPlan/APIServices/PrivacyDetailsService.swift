//
//  PrivacyDetailsService.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 5/10/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import Foundation

protocol PrivacyDetailsDelegate: class {
    func privacyDetailsRetrieved(outputMessage: String)
    func privacyDeatilsFailed(Error message: String)
}

class PrivacyDetailsService {
    
    weak var delegate: PrivacyDetailsDelegate?
    
    func getPrivacyDetails() {
        
        NetworkManager().apiCall(url: API.PRIVACY_DETAILS, http_method: HTTP_METHOD_GET, dict_body: nil, getParameters: nil) { (response) in
            if response["statusMessage"] as! String == "success" {
                let responseData = response["data"] as! NSDictionary
                let outputString = responseData["data"] as! String
                
                //Parse HTML String or if possible change the api response.
                
                self.delegate?.privacyDetailsRetrieved(outputMessage: outputString)
            } else {
                let error = "Error in API Call"
                self.delegate?.privacyDeatilsFailed(Error: error)
            }
            
        }
    }
}
