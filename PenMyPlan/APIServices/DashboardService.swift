//
//  DashboardService.swift
//  PenMyPlan
//
//  Created by PenMyPlan on 5/10/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import Foundation

protocol DashboardServiceDelegate: class {
    func dashboardServiceSuccessful(bannerCards: [Card], iosCards: [Card])
    func dashboardServiceFailed(Error message: String)
}

class DashboardService {
    
    weak var delegate : DashboardServiceDelegate?
    
    func getDashboardDetails() {
        
        NetworkManager().apiCall(url: API.DASHBOARD_DETAILS, http_method: HTTP_METHOD_GET, dict_body: nil, getParameters: nil) { (response) in
            if response["statusMessage"] as! String == "fail" {
                let err = response["error"] as! NSError
                self.delegate?.dashboardServiceFailed(Error: err.description)
            } else {
                let responseData = response["data"] as! NSDictionary
                let responseInnerData = responseData["data"] as! NSDictionary
                let iosBanner = responseInnerData["iOSBanner"] as! [NSDictionary]
                let iosCard = responseInnerData["iOSCard"] as! [NSDictionary]
                var bannerCards = [Card]()
                var iosCards = [Card]()
                
                for item in iosBanner {
                    var card = Card()
                    card.id = item["id"] as? Int
                    card.image = item["image"] as? String
                    card.name = item["name"] as? String
                    card.section = item["section"] as? Int
                    card.type = item["type"] as? String
                    bannerCards.append(card)
                }
                
                for item in iosCard {
                    var card = Card()
                    card.id = item["id"] as? Int
                    card.image = item["image"] as? String
                    card.name = item["name"] as? String
                    card.section = item["section"] as? Int
                    card.type = item["type"] as? String
                    iosCards.append(card)
                }
                self.delegate?.dashboardServiceSuccessful(bannerCards: bannerCards, iosCards: iosCards)
            }
        }
    }
}
