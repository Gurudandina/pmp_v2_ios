//
//  PopUpViewController.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 4/17/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit

class PopUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Button action functions
    @IBAction func closeClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
