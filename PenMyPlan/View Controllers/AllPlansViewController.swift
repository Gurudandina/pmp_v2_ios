//
//  AllPlansViewController.swift
//  PenMyPlan
//
//  Created by PenMyPlan on 5/11/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit

class AllPlansViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
