//
//  ThankYouViewController.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 4/18/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit

class ThankYouViewController: UIViewController {

    @IBOutlet weak var emailID: UITextField!
    @IBOutlet weak var fullName: UITextField!
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tapGesture.addTarget(self, action: #selector(self.tapChange))
    }
    
    // MARK: - Button action functions
    @IBAction func closeClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Private functions
    @objc func tapChange(){
        self.emailID.resignFirstResponder()
        self.fullName.resignFirstResponder()
    }

}
