//
//  AllTopDealsViewController.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 5/11/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit

class AllTopDealsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
