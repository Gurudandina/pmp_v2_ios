//
//  MoodOfTravelViewController.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 3/28/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit
import SpriteKit
import Magnetic

class MoodOfTravelViewController: UIViewController {

    @IBOutlet weak var selectedCountLabel: UILabel!
    @IBOutlet weak var magneticView: MagneticView!
    var magnetic : Magnetic?
    var magneticDelegate: MagneticDelegate?
    var selectedItems = [String]()
    let nodeColorSelected = UIColor.init(hexString: "#E48510")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        magneticView.scene?.backgroundColor = UIColor.init(hexString: "#F7F6F1")
        magnetic = magneticView.magnetic
        
        self.addNode(withText: "Business")
        self.addNode(withText: "Sunny & Beach")
        self.addNode(withText: "Nature & Landscape")
        self.addNode(withText: "Simple & Cosy")
        self.addNode(withText: "Luxury")
        self.addNode(withText: "Practical")
        
        magnetic?.magneticDelegate = self
        magneticDelegate = self
    }
    
    func addNode(withText: String) {
        let node = Node(text: withText, image: nil, color: .white, radius: 50)
        node.label.fontColor = UIColor.black
        node.label.fontName = "OpenSans-SemiBold"
        magnetic?.addChild(node)
    }

}

extension MoodOfTravelViewController: MagneticDelegate {
    func magnetic(_ magnetic: Magnetic, didSelect node: Node) {
        node.color = self.nodeColorSelected
        selectedItems.append(node.text!)
        self.selectedCountLabel.text = "\(selectedItems.count) Selected"
        print(selectedItems)
    }
    
    func magnetic(_ magnetic: Magnetic, didDeselect node: Node) {
        node.color = UIColor.white
        if let index = selectedItems.index(of: node.text!) {
            selectedItems.remove(at: index)
        }
        self.selectedCountLabel.text = "\(selectedItems.count) Selected"
        print(selectedItems)
    }
    
}
