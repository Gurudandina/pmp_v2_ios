//
//  HelpMainViewController.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 2/26/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit
import Presentr

class HelpMainViewController: UIViewController, PresentrDelegate {

    // MARK: - Variables and Outlets
    @IBOutlet var tapGestureClick: UITapGestureRecognizer!
    
    let height1:Float = 300.0
    let height2:Float = 480.0
    let height3:Float = 746.0
    
    let presentr = Presentr(presentationType: .bottomHalf)
    let presentr2 = Presentr(presentationType: .bottomHalf)
    let presentr3 = Presentr(presentationType: .bottomHalf)
    
    // MARK: - View Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tapGestureClick.addTarget(self, action: #selector(self.tapChange))
        
        presentr2.keyboardTranslationType = .moveUp

        let width = ModalSize.full
        let height = ModalSize.custom(size: height1)
        let bottomY = self.view.frame.maxY
        let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y: bottomY - CGFloat(height1)))
        presentr.presentationType = .custom(width: width, height: height, center: center)
        presentr.roundCorners = true
        presentr.cornerRadius = 10
        
        let width2 = ModalSize.full
        let height2 = ModalSize.custom(size: self.height2)
        let bottomY2 = self.view.frame.maxY
        let center2 = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y: bottomY2 - CGFloat(self.height2)))
        presentr2.presentationType = .custom(width: width2, height: height2, center: center2)
        presentr2.roundCorners = true
        presentr2.cornerRadius = 10
        
        let width3 = ModalSize.full
        let height3 = ModalSize.custom(size: self.height3)
        let bottomY3 = self.view.frame.maxY
        let center3 = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y: bottomY3 - CGFloat(self.height3)))
        presentr3.presentationType = .custom(width: width3, height: height3, center: center3)
        presentr3.roundCorners = true
        presentr3.cornerRadius = 10
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Button action functions
    @IBAction func closeClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func callHelpClicked(_ sender: Any) {
        let phoneNumber = 1234567890
        if let phoneCallURL:NSURL = NSURL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL as URL)) {
                application.open(phoneCallURL as URL, options: [:], completionHandler: nil)
            }
        }
    }
    
    @IBAction func contactUsClicked(_ sender: Any) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "PopUpViewController") as! PopUpViewController
        customPresentViewController(presentr, viewController: controller, animated: true, completion: nil)
    }
    
    @IBAction func emailClicked(_ sender: Any) {
        let controller2 = storyboard?.instantiateViewController(withIdentifier: "ThankYouViewController") as! ThankYouViewController
        customPresentViewController(presentr2, viewController: controller2, animated: true, completion: nil)
    }
    
    @IBAction func privacyPolicyClicked(_ sender: Any) {
        let controller3 = storyboard?.instantiateViewController(withIdentifier: "PrivacyViewController") as! PrivacyViewController
        customPresentViewController(presentr3, viewController: controller3, animated: true, completion: nil)
    }
    
    @IBAction func unwindToHelpMainScreen(segue:UIStoryboardSegue) {}
    
    // MARK: - Private functions
    @objc func tapChange() {
        
    }
    
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        return true
    }

}
