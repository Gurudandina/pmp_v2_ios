//
//  PrivacyViewController.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 4/20/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit
import Presentr

class PrivacyViewController: UIViewController, PrivacyDetailsDelegate {

    @IBOutlet weak var privacyDetailsTextView: UITextView!
    let privacyService = PrivacyDetailsService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        privacyService.delegate = self
        privacyService.getPrivacyDetails()
    }

    // MARK: - Button action functions
    @IBAction func closeClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func acceptClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Privacy Details delegate functions
    func privacyDetailsRetrieved(outputMessage: String) {
        DispatchQueue.main.async {
            self.privacyDetailsTextView.text = outputMessage
        }
    }
    
    func privacyDeatilsFailed(Error message: String) {
        AlertFunction.showAlert(withTitle: "Alert", message: message, forTarget: self)
    }
}
