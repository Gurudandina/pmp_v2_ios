//
//  ExploreCell2CollectionViewCell.swift
//  PenMyPlan
//
//  Created by PenMyPlan on 5/7/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit

class ExploreCell2CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var dealRateLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var dealsLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleNameLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}
