//
//  ExploreCell3CollectionViewCell.swift
//  PenMyPlan
//
//  Created by PenMyPlan on 5/11/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit

class ExploreCell3CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var title3: UILabel!
    @IBOutlet weak var title2: UILabel!
    @IBOutlet weak var title1: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - Button action functions
    @IBAction func favClicked(_ sender: Any) {
        
    }
    
}
