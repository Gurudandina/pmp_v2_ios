//
//  ExploreCell4CollectionViewCell.swift
//  PenMyPlan
//
//  Created by PenMyPlan on 5/11/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit

class ExploreCell4CollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceButton: UIButton!
    
    // MARK: - Button action functions
    @IBAction func priceButtonClicked(_ sender: Any) {
    }
    
}
