//
//  ExploreCell1CollectionViewCell.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 4/30/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit

class ExploreCell1CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    
    func loadNameAndImage(withCard image: Card) {
        if let nameString = image.name {
            self.nameLabel.text = nameString
        }
        if let imageURL = image.image {
            
            if let imageFromCache = imageCache.object(forKey: imageURL as AnyObject) as? UIImage {
                self.imageView.image = imageFromCache
                return
            }
            
            let url = URL(string: imageURL)
            URLSession.shared.dataTask(with: url!) { (data, response, error) in
                if error != nil {
                    print("Error in Image API call")
                } else {
                    DispatchQueue.main.async {
                        if let imageToCache = UIImage(data: data!) {
                            self.imageCache.setObject(imageToCache, forKey: imageURL as AnyObject)
                            self.imageView.image = UIImage(data: data!)
                        } else {
                            self.imageView.image = UIImage(named: "travel1")
                        }
                    }
                }
            }.resume()
            
        }
    }
    
}
