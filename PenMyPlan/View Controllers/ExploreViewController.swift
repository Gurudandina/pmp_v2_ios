//
//  ExploreViewController.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 3/26/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit

class ExploreViewController: UIViewController {
    
    @IBOutlet weak var bannerButton: UIButton!
    @IBOutlet weak var collectionView4: UICollectionView!
    @IBOutlet weak var collectionView3: UICollectionView!
    @IBOutlet weak var collectionView2: UICollectionView!
    @IBOutlet weak var collectionView1: UICollectionView!
    let dashboardService = DashboardService()
    var collection1Images = [Card]()
    var collection2Images = [Card]()
    let imageCache1 = NSCache<AnyObject, AnyObject>()

    override func viewDidLoad() {
        super.viewDidLoad()
        dashboardService.delegate = self
        dashboardService.getDashboardDetails()
    }
    
    // MARK: - Button action functions
    @IBAction func bannerClicked(_ sender: UIButton) {
        let bannerView = storyboard?.instantiateViewController(withIdentifier: "BannerViewController") as! BannerViewController
        self.present(bannerView, animated: true, completion: nil)
    }
    
    @IBAction func topDealsSeeAllClicked(_ sender: Any) {
        let topDealsView = storyboard?.instantiateViewController(withIdentifier: "AllTopDealsViewController") as! AllTopDealsViewController
        self.present(topDealsView, animated: true, completion: nil)
    }
    
    @IBAction func plansSeeAllClicked(_ sender: Any) {
        let plansView = storyboard?.instantiateViewController(withIdentifier: "AllPlansViewController") as! AllPlansViewController
        self.present(plansView, animated: true, completion: nil)
    }
    
    @IBAction func experiencesSeeAllClicked(_ sender: Any) {
        let expView = storyboard?.instantiateViewController(withIdentifier: "AllExperiencesViewController") as! AllExperiencesViewController
        self.present(expView, animated: true, completion: nil)
    }
    
    // MARK: - Private functions
    func changeBannerButton(WithInfo card: Card){
        if let imageURL = card.image {
            
            if let imageFromCache = imageCache1.object(forKey: imageURL as AnyObject) as? UIImage {
                self.bannerButton.imageView?.image = imageFromCache
                return
            }
            
            let url = URL(string: imageURL)
            URLSession.shared.dataTask(with: url!) { (data, response, error) in
                if error != nil {
                    print("Error in Image API call")
                } else {
                    DispatchQueue.main.async {
                        if let imageToCache = UIImage(data: data!) {
                            self.imageCache1.setObject(imageToCache, forKey: imageURL as AnyObject)
                            let img = UIImage(data: data!)
                            self.bannerButton.setImage(img, for: UIControlState.normal)
                        } else {
                            let img = UIImage(named: "travel1")
                            self.bannerButton.setImage(img, for: UIControlState.normal)
                        }
                    }
                }
            }.resume()
        }
    }
    
}

// MARK: - Collection View Extension
extension ExploreViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 {
            return self.collection1Images.count
        } else {
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 0 {
            let cell1 = collectionView.dequeue(ExploreCell1CollectionViewCell.self, for: indexPath)!
            cell1.customize(cell1)
            cell1.loadNameAndImage(withCard: self.collection1Images[indexPath.row])
            return cell1
        } else if collectionView.tag == 1 {
            let cell2 = collectionView.dequeue(ExploreCell2CollectionViewCell.self, for: indexPath)!
            cell2.customize(cell2)
            return cell2
        } else if collectionView.tag == 2 {
            let cell3 = collectionView.dequeue(ExploreCell3CollectionViewCell.self, for: indexPath)!
            cell3.customize(cell3)
            return cell3
        } else {
            let cell4 = collectionView.dequeue(ExploreCell4CollectionViewCell.self, for: indexPath)!
            cell4.customize(cell4)
            return cell4
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 {
            
        } else if collectionView.tag == 1 {
            let topDealsView = storyboard?.instantiateViewController(withIdentifier: "AllTopDealsViewController") as! AllTopDealsViewController
            self.present(topDealsView, animated: true, completion: nil)
        } else if collectionView.tag == 2 {
            let plansView = storyboard?.instantiateViewController(withIdentifier: "AllPlansViewController") as! AllPlansViewController
            self.present(plansView, animated: true, completion: nil)
        } else {
            let expView = storyboard?.instantiateViewController(withIdentifier: "AllExperiencesViewController") as! AllExperiencesViewController
            self.present(expView, animated: true, completion: nil)
        }
    }
    
}

// MARK: - Dashboard Service Delegate extension
extension ExploreViewController: DashboardServiceDelegate {
    
    func dashboardServiceSuccessful(bannerCards: [Card], iosCards: [Card]) {
        self.collection1Images = iosCards
        self.changeBannerButton(WithInfo: bannerCards[0])
        DispatchQueue.main.async {
            self.collectionView1.reloadData()
        }
    }
    
    func dashboardServiceFailed(Error message: String) {
        AlertFunction.showAlert(withTitle: "Alert", message: message, forTarget: self)
    }
    
    
}

// MARK: - Generic functions
protocol CollectionViewType {
    
    func dequeue<T: UICollectionViewCell>(_ cellClass: T.Type, for indexPath: IndexPath) -> T?
}

extension UICollectionView: CollectionViewType {
    
    func dequeue<T: AnyObject>(_ cellClass: T.Type, for indexPath: IndexPath) -> T? {
        return dequeueReusableCell(withReuseIdentifier: String(describing: cellClass), for: indexPath) as? T
    }
}

extension UICollectionViewCell {
    
    func customize<T: UICollectionViewCell>(_ cellForCustomize: T) {
        cellForCustomize.contentView.layer.cornerRadius = 10.0
        cellForCustomize.contentView.layer.cornerRadius = 4.0
        cellForCustomize.contentView.layer.borderWidth = 1.0
        cellForCustomize.contentView.layer.borderColor = UIColor.clear.cgColor
        cellForCustomize.contentView.layer.masksToBounds = false
        cellForCustomize.layer.cornerRadius = 10.0
        cellForCustomize.layer.shadowColor = UIColor.gray.cgColor
        cellForCustomize.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        cellForCustomize.layer.shadowRadius = 2.0
        cellForCustomize.layer.shadowOpacity = 0.5
        cellForCustomize.layer.masksToBounds = false
        cellForCustomize.layer.shadowPath = UIBezierPath(roundedRect: cellForCustomize.bounds, cornerRadius: cellForCustomize.contentView.layer.cornerRadius).cgPath
        
    }
}
