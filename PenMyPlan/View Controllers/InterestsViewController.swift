//
//  InterestsViewController.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 3/27/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit
import SpriteKit
import Magnetic


class InterestsViewController: UIViewController {

    @IBOutlet weak var selectedCountLabel: UILabel!
    @IBOutlet weak var magneticView: MagneticView!
    var magnetic : Magnetic?
    var selectedItems = [String]()
    var magneticDelegate: MagneticDelegate?
    let nodeColorSelected = UIColor.init(hexString: "#E48510")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        magneticView.scene?.backgroundColor = UIColor.init(hexString: "#F7F6F1")
        
        magnetic = magneticView.magnetic
        
        self.addNode(withText: "Shopping")
        self.addNode(withText: "Skiing")
        self.addNode(withText: "Cultural")
        self.addNode(withText: "Adventure")
        self.addNode(withText: "Hiking")
        self.addNode(withText: "Sailing")
        self.addNode(withText: "Historical")
        
        magnetic?.magneticDelegate = self
        magneticDelegate = self
    }
    
    func addNode(withText: String) {
        let node = Node(text: withText, image: nil, color: .white, radius: 50)
        node.label.fontColor = UIColor.black
        node.label.fontName = "OpenSans-SemiBold"
        magnetic?.addChild(node)
    }

}

extension InterestsViewController : MagneticDelegate {
    
    func magnetic(_ magnetic: Magnetic, didSelect node: Node) {
        node.color = self.nodeColorSelected
        selectedItems.append(node.text!)
        self.selectedCountLabel.text = "\(selectedItems.count) Selected"
        print(selectedItems)
    }
    
    func magnetic(_ magnetic: Magnetic, didDeselect node: Node) {
        node.color = UIColor.white
        if let index = selectedItems.index(of: node.text!) {
            selectedItems.remove(at: index)
        }
        self.selectedCountLabel.text = "\(selectedItems.count) Selected"
        print(selectedItems)
    }
    
}
