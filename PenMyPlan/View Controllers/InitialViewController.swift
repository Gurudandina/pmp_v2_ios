//
//  InitialViewController.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 4/9/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit
import CHIPageControl
import FacebookCore
import FBSDKCoreKit
import FBSDKLoginKit
import AccountKit
import CoreLocation
import KeychainSwift
import Presentr

class InitialViewController: UIViewController, PresentrDelegate {

    @IBOutlet weak var imagePageControl: myPageControl!
    @IBOutlet weak var animated_control: CHIPageControlJaloro!
    @IBOutlet weak var imgScrollView: UIScrollView!
    let imgArray = ["rectangle-copy","ic_background","travel3"]
    
    fileprivate var _accountKit : AKFAccountKit?
    fileprivate var _pendingLoginViewController: AKFViewController?
    fileprivate var _authorizationCode: String?
    fileprivate var _userLoginStatus = false
    let loginService = LoginService()
    var locationManager : CLLocationManager!
    var userLocationCoordinates = CLLocation()
    var keychainWrapper = KeychainSwift()
    let height2:Float = 480.0
    let presentr2 = Presentr(presentationType: .bottomHalf)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginService.delegate = self
        presentr2.keyboardTranslationType = .moveUp
        
        let width2 = ModalSize.full
        let height2 = ModalSize.custom(size: self.height2)
        let bottomY2 = self.view.frame.maxY
        let center2 = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y: bottomY2 - CGFloat(self.height2)))
        presentr2.presentationType = .custom(width: width2, height: height2, center: center2)
        presentr2.roundCorners = true
        presentr2.cornerRadius = 10
        
        //Checking for Location services
        if CLLocationManager.locationServicesEnabled() {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        //Checking if an account kit is already present.
        if _accountKit == nil {
            _accountKit = AKFAccountKit(responseType: .accessToken)
        }
        _pendingLoginViewController = _accountKit!.viewControllerForLoginResume()
        _pendingLoginViewController?.delegate = self
        
        _userLoginStatus = _accountKit?.currentAccessToken != nil
        if _userLoginStatus {
            print("User Already Logged In, Get Account Kit details and Navigate to Explore Screen Dashboard")
            UserDefaults.standard.set(false, forKey: Keys_General.Sign_check)
            self.getAccountKitDetails()
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.loadScroll()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.imagePageControl.updateDots()
        self.imagePageControl.transform = CGAffineTransform(scaleX: 1, y: 1)
    }
    
    // MARK: - Private functions
    func loadScroll() {
        let pageCount : CGFloat = CGFloat(imgArray.count)
        
        imgScrollView.backgroundColor = UIColor.clear
        imgScrollView.delegate = self
        imgScrollView.isPagingEnabled = true
        imgScrollView.contentSize = CGSize(width: imgScrollView.frame.size.width * pageCount, height: imgScrollView.frame.size.height)
        imgScrollView.showsHorizontalScrollIndicator = false
        
        imagePageControl.numberOfPages = Int(pageCount)
        imagePageControl.addTarget(self, action: #selector(self.pageChanged), for: .valueChanged)
        
        for i in 0..<Int(pageCount) {
            let image = UIImageView(frame: CGRect(x: self.imgScrollView.frame.size.width * CGFloat(i), y : 0, width :  self.imgScrollView.frame.size.width, height: self.imgScrollView.frame.size.height))
            image.image = UIImage(named: imgArray[i])!
            image.contentMode = UIViewContentMode.scaleToFill
            self.imgScrollView.addSubview(image)
        }
    }
    
    func prepareLoginViewController(loginViewController: AKFViewController) {
        let primaryCol = UIColor(named: "Yellow")
        let backImage = UIImage(named: "ic_background")
        loginViewController.uiManager = AKFSkinManager(skinType:AKFSkinType(rawValue: 2)!,
                                                       primaryColor:primaryCol,
                                                       backgroundImage:backImage,
                                                       backgroundTint:AKFBackgroundTint(rawValue: 0)!,
                                                       tintIntensity:0.55)
        loginViewController.delegate = self
    }
    
    func getAccountKitDetails() {
        _accountKit?.requestAccount {(account, error) in
            if error != nil {
                print("Error while requesting Account Kit details: \nError: \(error!.localizedDescription)")
            } else {
                self.navigateWithDetails(phone: account!.phoneNumber!.stringRepresentation())
            }
        }
    }
    
    func navigateWithDetails(phone: String) {
        //Saving user's location
        let lat = Double(self.userLocationCoordinates.coordinate.latitude)
        let lon = Double(self.userLocationCoordinates.coordinate.longitude)
        UserDefaults.standard.set(lat, forKey: Keys_General.Location_Latitude)
        UserDefaults.standard.set(lon, forKey: Keys_General.Location_Longitude)
        
        if UserDefaults.standard.bool(forKey: Keys_General.Sign_check) {
            //SignUp - Save data and navigate to Enter Name and Email Screen
            self.keychainWrapper.set(phone, forKey: Keys_General.Mobile_Number)
            
            let controller2 = storyboard?.instantiateViewController(withIdentifier: "ThankYouViewController") as! ThankYouViewController
            customPresentViewController(presentr2, viewController: controller2, animated: true, completion: nil)
            
        } else {
            //SignIn - Navigate to Explore screen - Dashboard
            loginService.signInWithMobileNumber()
        }
        
    }
    
    // MARK: - Button action functions
    @IBAction func helpClicked(_ sender: Any) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let helpView = storyBoard.instantiateViewController(withIdentifier: "HelpMainViewController") as! HelpMainViewController
        self.present(helpView, animated: true, completion: nil)
    }
    
    @IBAction func pageChanged(_ sender: Any) {
        self.imagePageControl.updateDots()
        let pageNumber = imagePageControl.currentPage
        var frame = imgScrollView.frame
        frame.origin.x = frame.size.width * CGFloat(pageNumber)
        frame.origin.y = 0
        imgScrollView.scrollRectToVisible(frame, animated: true)
    }
    
    @IBAction func signInWithMobileClicked(_ sender: UIButton) {
        let vc: AKFViewController = _accountKit!.viewControllerForPhoneLogin(with: nil, state: nil) as AKFViewController
        self.prepareLoginViewController(loginViewController: vc)
        self.present(vc as! UIViewController, animated: true, completion: nil)
    }
    
    @IBAction func facebookLoginClicked(_ sender: UIButton) {
        let facebookReadPermissions = ["public_profile", "email"]
        let facebookLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        
        facebookLoginManager.logIn(withReadPermissions: facebookReadPermissions, from:self, handler: { (result, error)  -> Void in
            if error != nil {
                print("\(String(describing: error))")
            } else if (result?.isCancelled)! {
                print("Cancelled")
            } else {
                print("Logged In")
            }
        })
        
        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, email"]).start(completionHandler: { (connection, result, error) -> Void in
            if error != nil {
                print(error!.localizedDescription)
            } else {
                print(result!)
            }
        })
    }
    
    // MARK: - Presentr Delegate functions
    func presentrShouldDismiss(keyboardShowing: Bool) -> Bool {
        return true
    }
    
}

// MARK: - Account Kit AKFViewControllerDelegate
extension InitialViewController: AKFViewControllerDelegate {
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        print("Did complete login with Authorization Code \(code) in State \(state)")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController)!, didFailWithError error: Error!) {
        print("Error \(error)")
    }
    
    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
        print("Did complete login with Access Token \(accessToken.tokenString) in State \(state)")
        UserDefaults.standard.set(true, forKey: Keys_General.LoginCheck)
        UserDefaults.standard.set(true, forKey: Keys_General.Sign_check)
        keychainWrapper.set(accessToken.tokenString, forKey: Keys_General.Account_Access_Token)
        UserDefaults.standard.synchronize()
        self.getAccountKitDetails()
    }
}

// MARK: - Scroll View Delegate extension
extension InitialViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.animated_control.set(progress: self.imagePageControl.currentPage, animated: true)
        self.imagePageControl.updateDots()
        let viewWidth: CGFloat = scrollView.frame.size.width
        let pageNumber = floor((scrollView.contentOffset.x - viewWidth / 50) / viewWidth) + 1
        imagePageControl.currentPage = Int(pageNumber)
    }
}

// MARK: - Login Service Delegate
extension InitialViewController: LoginServiceDelegate {
    func loginFailed(errorMessage: String) {
        print("Login Failed with Message: \(errorMessage)")
    }
    
    func loginSuccessful(from: String) {
        print("Login Successful from \(from)")
        if from == "SignIn" {
            let dashStoryboard = UIStoryboard.init(name: "Dashboard", bundle: nil)
            let exploreVC = dashStoryboard.instantiateViewController(withIdentifier: "ExploreViewController") as! ExploreViewController
            self.present(exploreVC, animated: true, completion: nil)
        }
    }
}

// MARK: - Location Service Delegate
extension InitialViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        userLocationCoordinates = locations[0]
    }
}
