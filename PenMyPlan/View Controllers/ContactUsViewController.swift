//
//  ContactUsViewController.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 3/2/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ContactUsViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var imageButton: UIButton!
    var imagePicker = UIImagePickerController()
    
    // MARK: - View Lifecycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
    }
    
    // MARK: - Button action functions
    @IBAction func closeButtonClicked(_ sender: Any) {
        performSegue(withIdentifier: "unwindToHelpMainScreen", sender: self)
    }
    
    @IBAction func imageButtonClicked(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    // MARK: - TextField Delegate functions
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 1 {
            if let textData = textField.text {
                if let floatTextfield = textField as? SkyFloatingLabelTextField {
                    if(textData.count < 3 || !textData.contains("@")) {
                        floatTextfield.errorMessage = "Please enter a valid Email"
                    }
                    else {
                        floatTextfield.errorMessage = ""
                    }
                }
            }
        }
        return true
    }
    
    // MARK: - Image Picker Controller Delegate functions
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.imageButton.setImage(pickedImage, for: .normal)
            self.imageButton.imageView?.contentMode = .scaleAspectFit
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

}
