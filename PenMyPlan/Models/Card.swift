//
//  Card.swift
//  PenMyPlan
//
//  Created by PenMyPlan on 5/10/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import Foundation

struct Card {
    var id : Int?
    var image : String?
    var name : String?
    var section : Int?
    var type : String?
}
