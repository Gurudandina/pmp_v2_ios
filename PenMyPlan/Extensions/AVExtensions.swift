//
//  AVExtensions.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 3/16/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import Foundation
import UIKit
import AdSupport

//Managing Production and Developement Environment
let isProd: String = UserDefaults.standard.value(forKey: Keys_General.isProdUserDefaultKey) as! String

//let API_DOMAIN_V2   = isProd == "0" ? "http://dev-apiv2.penmyplan.com/write/api/v2.0" : "https://api-v2.com"
//let API_DOMAIN_READ_V2   = isProd == "0" ? "http://dev-apiv2.penmyplan.com/read/api/v2.0" : "https://api-v2.com"

let API_DOMAIN_V2  = "http://dev-apiv2.penmyplan.com/write/api/v2.0"
let API_DOMAIN_READ_V2 = "http://dev-apiv2.penmyplan.com/read/api/v2.0"

public struct API {
    static let SIGNUP_URL = API_DOMAIN_V2 + "/signup"
    static let PROFILE_URL = API_DOMAIN_V2 + "/update_user_profile/1"
    static let GET_USER_PREFERENCE_INFO_URL = API_DOMAIN_READ_V2 + "/get_user_preference_info/"
    static let SIGNIN_URL = API_DOMAIN_V2  + "/signin"
    static let GET_ALL_DEALS = API_DOMAIN_READ_V2 + "get_all_deals/1/7" // 1 - PAGE NO , 7 - Limit - means no. of deals count
    static let GET_MEAL = API_DOMAIN_READ_V2 + "/get_all_meals/"
    static let BOOKING_API = API_DOMAIN_V2 + "/insert_booking"///insert_meal_booking
    static let MEAL_BOOKING_API = API_DOMAIN_V2 + "/insert_meal_booking"
    static let PRIVACY_DETAILS = API_DOMAIN_READ_V2 + "/get_terms_and_conditions"
    static let DASHBOARD_DETAILS = API_DOMAIN_READ_V2 + "/get_home_page/2"
}

public struct Global_Keys{
    static let API_KEY_SECRET = isProd == "0" ? "d5664869e1f3fbea2fd8387267c9e31c272be60e445bab5cda3fdd20f3242d65" : "981edd7c44ff82741a1fb5a9663b21008a59c346e686dbe99b454d5d09dc7092"
    static let HMAC_SALT = UIDevice.current.userInterfaceIdiom == .phone ? "71103351ce72120ed65cbdb82dae099a84e6360b46b9c65564f2b14b2206a730" : "e353365bf19c28e629c0147cc23134c947b98bf1345bdf4337ecfd6e4f55ad4b"
    //static let device_indentifier = OpenUDID.value()
    static let idfa = ASIdentifierManager.shared().advertisingIdentifier.uuidString
    static let idfv = UIDevice.current.identifierForVendor!.uuidString
}

let HTTP_METHOD_POST = "POST"
let HTTP_METHOD_GET = "GET"

let countryCodeDictionary = [
    "Abkhazia": "+7 840",
    "Afghanistan": "+93",
    "Albania": "+355",
    "Algeria": "+213",
    "American Samoa": "+1 684",
    "Andorra": "+376",
    "Angola": "+244",
    "Anguilla": "+1 264",
    "Antigua and Barbuda": "+1 268",
    "Argentina": "+54",
    "Armenia": "+374",
    "Aruba": "+297",
    "Ascension": "+247",
    "Australia": "+61",
    "Australian External Territories": "+672",
    "Austria": "+43",
    "Azerbaijan": "+994",
    "Bahamas": "+1 242",
    "Bahrain": "+973",
    "Bangladesh": "+880",
    "Barbados": "+1 246",
    "Barbuda": "+1 268",
    "Belarus": "+375",
    "Belgium": "+32",
    "Belize": "+501",
    "Benin": "+229",
    "Bermuda": "+1 441",
    "Bhutan": "+975",
    "Bolivia": "+591",
    "Bosnia and Herzegovina": "+387",
    "Botswana": "+267",
    "Brazil": "+55",
    "British Indian Ocean Territory": "+246",
    "British Virgin Islands": "+1 284",
    "Brunei": "+673",
    "Bulgaria": "+359",
    "Burkina Faso": "+226",
    "Burundi": "+257",
    "Cambodia": "+855",
    "Cameroon": "+237",
    "Cape Verde": "+238",
    "Cayman Islands": "+ 345",
    "Central African Republic": "+236",
    "Chad": "+235",
    "Chile": "+56",
    "China": "+86",
    "Christmas Island": "+61",
    "Cocos-Keeling Islands": "+61",
    "Colombia": "+57",
    "Comoros": "+269",
    "Congo": "+242",
    "Congo, Dem. Rep. of (Zaire)": "+243",
    "Cook Islands": "+682",
    "Costa Rica": "+506",
    "Croatia": "+385",
    "Cuba": "+53",
    "Curacao": "+599",
    "Cyprus": "+537",
    "Czech Republic": "+420",
    "Denmark": "+45",
    "Diego Garcia": "+246",
    "Djibouti": "+253",
    "Dominica": "+1 767",
    "Dominican Republic": "+1 809",
    "East Timor": "+670",
    "Easter Island": "+56",
    "Ecuador": "+593",
    "Egypt": "+20",
    "El Salvador": "+503",
    "Equatorial Guinea": "+240",
    "Eritrea": "+291",
    "Estonia": "+372",
    "Ethiopia": "+251",
    "Falkland Islands": "+500",
    "Faroe Islands": "+298",
    "Fiji": "+679",
    "Finland": "+358",
    "France": "+33",
    "French Antilles": "+596",
    "French Guiana": "+594",
    "French Polynesia": "+689",
    "Gabon": "+241",
    "Gambia": "+220",
    "Georgia": "+995",
    "Germany": "+49",
    "Ghana": "+233",
    "Gibraltar": "+350",
    "Greece": "+30",
    "Greenland": "+299",
    "Grenada": "+1 473",
    "Guadeloupe": "+590",
    "Guam": "+1 671",
    "Guatemala": "+502",
    "Guinea": "+224",
    "Guinea-Bissau": "+245",
    "Guyana": "+595",
    "Haiti": "+509",
    "Honduras": "+504",
    "Hong Kong SAR China": "+852",
    "Hungary": "+36",
    "Iceland": "+354",
    "India": "+91",
    "Indonesia": "+62",
    "Iran": "+98",
    "Iraq": "+964",
    "Ireland": "+353",
    "Israel": "+972",
    "Italy": "+39",
    "Ivory Coast": "+225",
    "Jamaica": "+1 876",
    "Japan": "+81",
    "Jordan": "+962",
    "Kazakhstan": "+7 7",
    "Kenya": "+254",
    "Kiribati": "+686",
    "Kuwait": "+965",
    "Kyrgyzstan": "+996",
    "Laos": "+856",
    "Latvia": "+371",
    "Lebanon": "+961",
    "Lesotho": "+266",
    "Liberia": "+231",
    "Libya": "+218",
    "Liechtenstein": "+423",
    "Lithuania": "+370",
    "Luxembourg": "+352",
    "Macau SAR China": "+853",
    "Macedonia": "+389",
    "Madagascar": "+261",
    "Malawi": "+265",
    "Malaysia": "+60",
    "Maldives": "+960",
    "Mali": "+223",
    "Malta": "+356",
    "Marshall Islands": "+692",
    "Martinique": "+596",
    "Mauritania": "+222",
    "Mauritius": "+230",
    "Mayotte": "+262",
    "Mexico": "+52",
    "Micronesia": "+691",
    "Midway Island": "+1 808",
    "Moldova": "+373",
    "Monaco": "+377",
    "Mongolia": "+976",
    "Montenegro": "+382",
    "Montserrat": "+1664",
    "Morocco": "+212",
    "Myanmar": "+95",
    "Namibia": "+264",
    "Nauru": "+674",
    "Nepal": "+977",
    "Netherlands": "+31",
    "Netherlands Antilles": "+599",
    "Nevis": "+1 869",
    "New Caledonia": "+687",
    "New Zealand": "+64",
    "Nicaragua": "+505",
    "Niger": "+227",
    "Nigeria": "+234",
    "Niue": "+683",
    "Norfolk Island": "+672",
    "North Korea": "+850",
    "Northern Mariana Islands": "+1 670",
    "Norway": "+47",
    "Oman": "+968",
    "Pakistan": "+92",
    "Palau": "+680",
    "Palestinian Territory": "+970",
    "Panama": "+507",
    "Papua New Guinea": "+675",
    "Paraguay": "+595",
    "Peru": "+51",
    "Philippines": "+63",
    "Poland": "+48",
    "Portugal": "+351",
    "Puerto Rico": "+1 787",
    "Qatar": "+974",
    "Reunion": "+262",
    "Romania": "+40",
    "Russia": "+7",
    "Rwanda": "+250",
    "Samoa": "+685",
    "San Marino": "+378",
    "Saudi Arabia": "+966",
    "Senegal": "+221",
    "Serbia": "+381",
    "Seychelles": "+248",
    "Sierra Leone": "+232",
    "Singapore": "+65",
    "Slovakia": "+421",
    "Slovenia": "+386",
    "Solomon Islands": "+677",
    "South Africa": "+27",
    "South Georgia and the South Sandwich Islands": "+500",
    "South Korea": "+82",
    "Spain": "+34",
    "Sri Lanka": "+94",
    "Sudan": "+249",
    "Suriname": "+597",
    "Swaziland": "+268",
    "Sweden": "+46",
    "Switzerland": "+41",
    "Syria": "+963",
    "Taiwan": "+886",
    "Tajikistan": "+992",
    "Tanzania": "+255",
    "Thailand": "+66",
    "Timor Leste": "+670",
    "Togo": "+228",
    "Tokelau": "+690",
    "Tonga": "+676",
    "Trinidad and Tobago": "+1 868",
    "Tunisia": "+216",
    "Turkey": "+90",
    "Turkmenistan": "+993",
    "Turks and Caicos Islands": "+1 649",
    "Tuvalu": "+688",
    "U.S. Virgin Islands": "+1 340",
    "Uganda": "+256",
    "Ukraine": "+380",
    "United Arab Emirates": "+971",
    "United Kingdom": "+44",
    "United States & Canada": "+1",
    "Uruguay": "+598",
    "Uzbekistan": "+998",
    "Vanuatu": "+678",
    "Venezuela": "+58",
    "Vietnam": "+84",
    "Wake Island": "+1 808",
    "Wallis and Futuna": "+681",
    "Yemen": "+967",
    "Zambia": "+260",
    "Zanzibar": "+255",
    "Zimbabwe": "+263"
] as Dictionary

public struct Keys_General {
    //Sets the current environment
    static let isProdUserDefaultKey = "isProdEnvironment"
    //Search screen
    static let pushtoken = "pushtoken"
    //Leaderboard screen
    static let isLeaderboardOption = "isLeaderboardOption"
    
    // Keys used for Saving Data and Flag checks
    static let User_Name = "userName"
    static let Mobile_Number = "mobileNumber"
    static let Email_Address = "emailAddress"
    static let LoginCheck = "loginCheck"
    static let Account_Access_Token = "accountKitAccessToken"
    static let Sign_check = "signCheck" //True - SignUp, False - SignIn
    static let User_Location_Coordinates = "userLocationCoordinates"
    static let Location_Latitude = "userLocationLatitude"
    static let Location_Longitude = "userLocationLongitude"
}

struct NotificationKey {
    static let Welcome = "kWelcomeNotif"
    static let APNS_Notification_Key = "kAPNSNotif"
}

public struct AppConstants {
    struct Path {
        static let Documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        static let Tmp = NSTemporaryDirectory()
    }
}

public class AlertFunction {
    class func showAlert(withTitle: String, message: String, forTarget: UIViewController) {
        let alert = UIAlertController(title: withTitle, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(alertAction)
        forTarget.present(alert, animated: true, completion: nil)
    }
}

public func getTimeStamp() -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.timeStyle = .none
    dateFormatter.dateStyle = .medium
    dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss Z"
    dateFormatter.locale = Locale(identifier: "en_US")
    return dateFormatter.string(from: Date())
}

func version() -> String {
    let ver = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
    return "\(ver)"
}

func formatCurrency(value: Double) -> String {
    let formatter = NumberFormatter()
    formatter.numberStyle = .decimal
    formatter.maximumFractionDigits = 0
    formatter.locale = Locale(identifier: Locale.current.identifier)
    let result = formatter.string(from: value as NSNumber)
    return result!
}

extension URLRequest{
    public mutating func defaultRequest(url: String, param: Data, httpMethod: String) {
        let paramMutable = param
        if httpMethod == "POST"{
            self.url = URL(string: url)
            self.httpBody = paramMutable//.data(using: .utf8)
        }
        else{
            var urlMutable = url
            url.contains("?") ? urlMutable.append("&\(paramMutable)") : urlMutable.append("?\(paramMutable)")
            self.url = URL(string: urlMutable)
        }
        self.httpMethod = httpMethod
    }
    
    public mutating func defaultGetRequest(url: String){
        self.timeoutInterval = 240
        self.cachePolicy = .reloadIgnoringCacheData
    }
}

extension UIViewController{
    func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UIView{
    func addGradiantLayer() {
        let caGrdiantLayer = CAGradientLayer()
        caGrdiantLayer.frame = self.bounds
        caGrdiantLayer.startPoint = CGPoint(x: 0, y: 0)
        caGrdiantLayer.endPoint = CGPoint(x: 1.0, y: 0)
        caGrdiantLayer.colors = [UIColor.init(red: 238/255.0, green: 57/255.0, blue: 83/255.0, alpha: 1.0).cgColor, UIColor.init(red: 95/255.0, green: 195/255.0, blue: 237/255.0, alpha: 1.0).cgColor, UIColor.init(red: 105/255.0, green: 208/255.0, blue: 110/255.0, alpha: 1.0).cgColor]
        self.layer.addSublayer(caGrdiantLayer)
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
