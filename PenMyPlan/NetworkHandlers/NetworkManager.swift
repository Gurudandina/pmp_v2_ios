//
//  NetworkManager.swift
//  PenMyPlan
//
//  Created by Navya Jagadish on 3/16/18.
//  Copyright © 2018 PenMyPlan. All rights reserved.
//

import Foundation

public struct NetworkManager {
    public func apiCall(url: String, http_method: String, dict_body: NSDictionary!, getParameters: NSString!, successCallBack : @escaping (_ dictResponse: NSDictionary) -> Void) {
        
        var dictionaryResponse = Dictionary<String, Any>()
        
        guard let url = URL(string: url) else {
            return
        }
        
        var request = URLRequest(url: url)
        
        if http_method == HTTP_METHOD_POST {
            request.httpMethod = HTTP_METHOD_POST
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            do {
                let requestData = try JSONSerialization.data(withJSONObject: dict_body, options: [])
                request.httpBody = requestData
            } catch {}
        } else {
            request.httpMethod = HTTP_METHOD_GET
        }
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request) { (data, response, error) in
            
            if response != nil {
                let httpResponse = response as! HTTPURLResponse
                let statusCode = httpResponse.statusCode
                
                if statusCode < 300 {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) as? NSDictionary {
                            dictionaryResponse = ["statusCode":"\(statusCode)", "statusMessage":"success", "data":json]
                            successCallBack(dictionaryResponse as NSDictionary)
                        } else if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray {
                            dictionaryResponse = ["statusCode":"\(statusCode)", "statusMessage":"success", "data":json]
                            successCallBack(dictionaryResponse as NSDictionary)
                        } else {
                            dictionaryResponse = ["statusCode":"\(statusCode)", "statusMessage":"success", "data":""]
                            successCallBack(dictionaryResponse as NSDictionary)
                        }
                    } catch {
                        dictionaryResponse = ["statusCode":"\(statusCode)", "statusMessage":"fail", "data":"", "error":error]
                        successCallBack(dictionaryResponse as NSDictionary)
                    }
                    print("API Call made successfully made with the status code \(statusCode)")
                } else {
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data!, options: [.allowFragments]) as? NSDictionary {
                            dictionaryResponse = ["statusCode":"\(statusCode)", "statusMessage":"fail", "data":json]
                            successCallBack(dictionaryResponse as NSDictionary)
                        } else if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? NSArray {
                            dictionaryResponse = ["statusCode":"\(statusCode)", "statusMessage":"fail", "data":json]
                            successCallBack(dictionaryResponse as NSDictionary)
                        } else {
                            dictionaryResponse = ["statusCode":"\(statusCode)", "statusMessage":"fail", "data":""]
                            successCallBack(dictionaryResponse as NSDictionary)
                        }
                    } catch {
                        dictionaryResponse = ["statusCode":"\(statusCode)", "statusMessage":"fail", "data":"", "error":error]
                        successCallBack(dictionaryResponse as NSDictionary)
                    }
                    print("API Call failed with status code \(statusCode)")
                }
            } else {
                dictionaryResponse = ["statusCode":"\(0)", "statusMessage":"fail", "data":""]
                successCallBack(dictionaryResponse as NSDictionary)
                print("API Call failed with no response")
            }
        }
        task.resume()
    }
}
